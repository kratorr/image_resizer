from PIL import Image
import requests
from tempfile import NamedTemporaryFile
from io import BytesIO


def resize_image(image, width, height, size_limit):
    input_image = Image.open(image)
    resized_image = input_image.resize((width, height)).convert('RGB')
    tmpfile = NamedTemporaryFile()
    out = BytesIO()
    resized_image.save(out, format='JPEG')
    quality = 75
    with BytesIO() as buffer:
            resized_image.save(buffer, format="JPEG")
            data = buffer.getvalue()
    filesize = len(data)
  
    while True:
        if filesize < size_limit or quality == 1:
            resized_image.save(tmpfile.name + '.jpg', format='JPEG', quality=quality + 1)
            break
        else:
            with BytesIO() as buffer:
                resized_image.save(buffer, format="JPEG", quality=quality)
                data = buffer.getvalue()
            filesize = len(data)
            quality = quality - 1
    return tmpfile.name + '.jpg'

    
    
    


def reduce_img_size_by_limit(filename, limit_bytes):
    img = Image.open(filename)

    quality = 75

    with BytesIO() as buffer:
            img.save(buffer, format="JPEG")
            data = buffer.getvalue()
    filesize = len(data)
    print(filesize)

    while True:
        if filesize < 15000:
            print('GOOD return buffer')
            break
        else:
            print('here')
            with BytesIO() as buffer:
                img.save(buffer, format="JPEG", quality=quality)
                data = buffer.getvalue()
            filesize = len(data)
            print(filesize)
            quality = quality - 1
            print(quality)
    return buffer




def download_image(url):
    request = requests.get(url, stream=True)
    file_name = url.split('/')[-1]
    image = request.content
    return image, file_name